--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3
-- Dumped by pg_dump version 15.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: notify_messenger_messages(); Type: FUNCTION; Schema: public; Owner: app
--

CREATE FUNCTION public.notify_messenger_messages() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
            BEGIN
                PERFORM pg_notify('messenger_messages', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$;


ALTER FUNCTION public.notify_messenger_messages() OWNER TO app;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: author; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.author (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.author OWNER TO app;

--
-- Name: author_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.author_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.author_id_seq OWNER TO app;

--
-- Name: book; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.book (
    id integer NOT NULL,
    google_books_id character varying(255) NOT NULL,
    title text NOT NULL,
    subtitle text,
    publish_date date,
    description text,
    isbn10 character varying(255) DEFAULT NULL::character varying,
    isbn13 character varying(255) DEFAULT NULL::character varying,
    page_count integer,
    small_thumbnail character varying(255) DEFAULT NULL::character varying,
    thumbnail character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.book OWNER TO app;

--
-- Name: book_author; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.book_author (
    book_id integer NOT NULL,
    author_id integer NOT NULL
);


ALTER TABLE public.book_author OWNER TO app;

--
-- Name: book_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.book_id_seq OWNER TO app;

--
-- Name: book_publisher; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.book_publisher (
    book_id integer NOT NULL,
    publisher_id integer NOT NULL
);


ALTER TABLE public.book_publisher OWNER TO app;

--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO app;

--
-- Name: messenger_messages; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.messenger_messages (
    id bigint NOT NULL,
    body text NOT NULL,
    headers text NOT NULL,
    queue_name character varying(190) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    available_at timestamp(0) without time zone NOT NULL,
    delivered_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.messenger_messages OWNER TO app;

--
-- Name: COLUMN messenger_messages.created_at; Type: COMMENT; Schema: public; Owner: app
--

COMMENT ON COLUMN public.messenger_messages.created_at IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN messenger_messages.available_at; Type: COMMENT; Schema: public; Owner: app
--

COMMENT ON COLUMN public.messenger_messages.available_at IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN messenger_messages.delivered_at; Type: COMMENT; Schema: public; Owner: app
--

COMMENT ON COLUMN public.messenger_messages.delivered_at IS '(DC2Type:datetime_immutable)';


--
-- Name: messenger_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.messenger_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messenger_messages_id_seq OWNER TO app;

--
-- Name: messenger_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: app
--

ALTER SEQUENCE public.messenger_messages_id_seq OWNED BY public.messenger_messages.id;


--
-- Name: publisher; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.publisher (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.publisher OWNER TO app;

--
-- Name: publisher_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.publisher_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.publisher_id_seq OWNER TO app;

--
-- Name: status; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.status (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.status OWNER TO app;

--
-- Name: status_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.status_id_seq OWNER TO app;

--
-- Name: user; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    email character varying(180) NOT NULL,
    roles json NOT NULL,
    password character varying(255) NOT NULL,
    pseudo character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public."user" OWNER TO app;

--
-- Name: user_book; Type: TABLE; Schema: public; Owner: app
--

CREATE TABLE public.user_book (
    id integer NOT NULL,
    user_id integer,
    book_id integer,
    status_id integer,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    comment text,
    rating integer
);


ALTER TABLE public.user_book OWNER TO app;

--
-- Name: COLUMN user_book.created_at; Type: COMMENT; Schema: public; Owner: app
--

COMMENT ON COLUMN public.user_book.created_at IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN user_book.updated_at; Type: COMMENT; Schema: public; Owner: app
--

COMMENT ON COLUMN public.user_book.updated_at IS '(DC2Type:datetime_immutable)';


--
-- Name: user_book_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.user_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_book_id_seq OWNER TO app;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: app
--

CREATE SEQUENCE public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO app;

--
-- Name: messenger_messages id; Type: DEFAULT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.messenger_messages ALTER COLUMN id SET DEFAULT nextval('public.messenger_messages_id_seq'::regclass);


--
-- Name: author author_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.author
    ADD CONSTRAINT author_pkey PRIMARY KEY (id);


--
-- Name: book_author book_author_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.book_author
    ADD CONSTRAINT book_author_pkey PRIMARY KEY (book_id, author_id);


--
-- Name: book book_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.book
    ADD CONSTRAINT book_pkey PRIMARY KEY (id);


--
-- Name: book_publisher book_publisher_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.book_publisher
    ADD CONSTRAINT book_publisher_pkey PRIMARY KEY (book_id, publisher_id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: messenger_messages messenger_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.messenger_messages
    ADD CONSTRAINT messenger_messages_pkey PRIMARY KEY (id);


--
-- Name: publisher publisher_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.publisher
    ADD CONSTRAINT publisher_pkey PRIMARY KEY (id);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id);


--
-- Name: user_book user_book_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.user_book
    ADD CONSTRAINT user_book_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: idx_75ea56e016ba31db; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_75ea56e016ba31db ON public.messenger_messages USING btree (delivered_at);


--
-- Name: idx_75ea56e0e3bd61ce; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_75ea56e0e3bd61ce ON public.messenger_messages USING btree (available_at);


--
-- Name: idx_75ea56e0fb7336f0; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_75ea56e0fb7336f0 ON public.messenger_messages USING btree (queue_name);


--
-- Name: idx_8e46c30016a2b381; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_8e46c30016a2b381 ON public.book_publisher USING btree (book_id);


--
-- Name: idx_8e46c30040c86fce; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_8e46c30040c86fce ON public.book_publisher USING btree (publisher_id);


--
-- Name: idx_9478d34516a2b381; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_9478d34516a2b381 ON public.book_author USING btree (book_id);


--
-- Name: idx_9478d345f675f31b; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_9478d345f675f31b ON public.book_author USING btree (author_id);


--
-- Name: idx_b164eff816a2b381; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_b164eff816a2b381 ON public.user_book USING btree (book_id);


--
-- Name: idx_b164eff86bf700bd; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_b164eff86bf700bd ON public.user_book USING btree (status_id);


--
-- Name: idx_b164eff8a76ed395; Type: INDEX; Schema: public; Owner: app
--

CREATE INDEX idx_b164eff8a76ed395 ON public.user_book USING btree (user_id);


--
-- Name: uniq_8d93d649e7927c74; Type: INDEX; Schema: public; Owner: app
--

CREATE UNIQUE INDEX uniq_8d93d649e7927c74 ON public."user" USING btree (email);


--
-- Name: messenger_messages notify_trigger; Type: TRIGGER; Schema: public; Owner: app
--

CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON public.messenger_messages FOR EACH ROW EXECUTE FUNCTION public.notify_messenger_messages();


--
-- Name: book_publisher fk_8e46c30016a2b381; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.book_publisher
    ADD CONSTRAINT fk_8e46c30016a2b381 FOREIGN KEY (book_id) REFERENCES public.book(id) ON DELETE CASCADE;


--
-- Name: book_publisher fk_8e46c30040c86fce; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.book_publisher
    ADD CONSTRAINT fk_8e46c30040c86fce FOREIGN KEY (publisher_id) REFERENCES public.publisher(id) ON DELETE CASCADE;


--
-- Name: book_author fk_9478d34516a2b381; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.book_author
    ADD CONSTRAINT fk_9478d34516a2b381 FOREIGN KEY (book_id) REFERENCES public.book(id) ON DELETE CASCADE;


--
-- Name: book_author fk_9478d345f675f31b; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.book_author
    ADD CONSTRAINT fk_9478d345f675f31b FOREIGN KEY (author_id) REFERENCES public.author(id) ON DELETE CASCADE;


--
-- Name: user_book fk_b164eff816a2b381; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.user_book
    ADD CONSTRAINT fk_b164eff816a2b381 FOREIGN KEY (book_id) REFERENCES public.book(id);


--
-- Name: user_book fk_b164eff86bf700bd; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.user_book
    ADD CONSTRAINT fk_b164eff86bf700bd FOREIGN KEY (status_id) REFERENCES public.status(id);


--
-- Name: user_book fk_b164eff8a76ed395; Type: FK CONSTRAINT; Schema: public; Owner: app
--

ALTER TABLE ONLY public.user_book
    ADD CONSTRAINT fk_b164eff8a76ed395 FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- PostgreSQL database dump complete
--